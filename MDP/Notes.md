# MDPs and Bellman Equations

[TOC]

## Learning Goals

- Understand the Agent-Environment interface
- Understand what MDPs (Markov Decision Processes) are and how to interpret transition diagrams
- Understand Value Functions, Action-Value Functions, and Policy Functions
- Understand the Bellman Equations and Bellman Optimality Equations for value functions and action-value functions



## Agent-Environment interface

Agent（智能体）：进行学习和实施决策的机器

Environment（环境）：所有与其相互作用的事物

> State（状态）：从智能体角度看到的外接环境的状态



## MDP：Markov Decision Process

MDP是一个轨迹
$$
\begin{equation} 
S_0,A_0, R_1, S_1, A_1, R_2, S_2, A_2, R_3, S_3, A_3, R_4....   
\end{equation}
$$


MDP动态特性，我们定义$$p(s\prime, r|s, a)$$为一个概率值，表示t-1时刻在状态s，进行a动作之后得到奖励r并到达下一个状态$$s\prime$$状态
$$
p(s\prime,r|s, a) \doteq Pr(S_t=s\prime, R_t = r|S_{t-1}=s, A_{t-1}=a)
$$


状态转移概率：将MDP关于r积分， 状态s下，进行a动作，到达$$s\prime$$的概率，p:SxSxA -> [0,1]
$$
p(s\prime|s, a) \doteq \sum_{r\in R}p(s\prime,r|s, a)
$$
二元期望收益：$$r(s,a)$$， SxA -> [0, 1]
$$
r(s,a) = \sum_{r\in R}r\sum_{s\prime \in S}p(s\prime,r|s, a)
$$
三元期望收益：$$r(s,a,s\prime)$$, SxAxS -> [0,1]
$$
r(s,a,s\prime) = \sum_{r \in R}r\frac{p(s\prime, r|s,a)}{p(s\prime|s,a)}
$$
收益：是环境反馈给智能体的信号，$$r \in R$$，不是先验知识



目标：智能体目标是最大化收益累计和的概率期望，不是当前收益



> episode（分幕式）与持续性任务：
>
> 分幕式任务是有终止态，比如迷宫游戏，从一个起始位置开始，经过N步之后游戏胜利或者失败。每一次玩都是一幕。
>
> 持续性任务是没有终止态的，比如一个长期运行的机器人，T=无穷大。



回报：收益总和。我们的目的是长期收益最大化，也就是回报最大化
$$
G_t = R_{t+1} + R_{t+2} + R_{t+3} + ...
$$


期望折后回报：
$$
G_t = R_{t+1} + \gamma R_{t+2} + \gamma^2R_{t+3} + ...
    = \sum_{k=0}^\infty \gamma^kR_{t+k+1} 
$$


状态转移图:例子3.3的那种图



## 价值函数，动作价值函数，策略函数

策略： 可以分为随机性策略和确定性策略；

* 确定性策略是当到达状态s之后只能取动作a；是一种特殊的随机性策略；

* **随机性策略**：从状态s到每一个动作a的选择概率之间的映射，用概率表示，$$\pi(a|s) = Pr(A_t = a | S_t=s)$$



![image-20210919181020489](C:\Users\Administrator\OneDrive\study\Reinforcement Learning\reinforcement-learning\images\MDP traceback graph.png)

其中，MDP概念里的轨迹是回溯图中的一个确定的一个分叉。



状态价值函数： 在状态s下，执行策略$$\pi$$（随机策略）的话，获得回报的期望（加权平均）。$$v_\pi(s)$$，
$$
v_\pi(s) \doteq E_\pi(G_t|S_t =s) = E_\pi(\sum_{k=0}^{\infty}\gamma^kR_{t+k+1} | S_t = s)
$$


动作-状态价值函数：在策略$$\pi$$下，在状态s时采取动作a的价值记为$$q_\pi(s,a)$$，
$$
q_\pi(s,a) \doteq E_\pi(G_t|S_t = s,A_t = a) = E_\pi(\sum_{k=0}^{\infty}\gamma^kR_{t+k+1} | S_t = s,A_t = a)
$$
状态价值与动作-状态价值函数之间的关系：
$$
V_\pi(s) = \sum_a\pi(a|s)q_\pi(s,a)
$$


## Bellman方程，Bellman 最优方程

![image-20210919182939575](C:\Users\Administrator\OneDrive\study\Reinforcement Learning\reinforcement-learning\images\MDP traceback graph r is random variable.png)

在s状态下，执行a动作，一定能到$$s\prime$$状态，但是得到的reward（收益）是随机的。

$$V_\pi(s)$$和$$G_t$$是类似定义的
$$
G_t = r + \gamma G_{t+1}
$$
所以，$$q_\pi(s,a)$$与$$V_\pi(s\prime)$$之间的关系是
$$
q_\pi(s,a) = \sum_{r,s\prime}p(s\prime, r|s,a)(r + \gamma V_\pi(s\prime))
$$
而上一节可知$$V_\pi(s) = \sum_a\pi(a|s)q_\pi(s,a)$$

因此，
$$
V_\pi(s) = \sum_a\pi(a|s)\sum_{r,s\prime}p(s\prime, r|s,a)(r + \gamma V_\pi(s\prime))
$$
前一个状态的价值函数与下一刻的状态函数之间的关系式有了，这个就是**贝尔曼方程**
$$
q_\pi(s,a) = \sum_{s\prime, r}p(s\prime, r|s,a)[r + \gamma\sum_{a\prime}\pi(a\prime|s\prime)q_\pi(s\prime, a\prime)]
$$
前一个状态-动作价值函数与下一刻的状态-动作价值函数的关系称为**贝尔曼期望方程**

























